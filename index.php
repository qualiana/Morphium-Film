<?php
/**
 * Index Template
 *
 * This is the index template.  Technically, it is the "posts page" template.  It is used when a visitor is on
 * the page assigned to show a site's late3st blog posts or on a category/tag/archive/taxonomy page if a more
 * specific template is not available.in
 *
 */
get_header(); ?>



        <?php if( is_front_page() && have_rows('sections') ) :?>
            <main id="front_page" class="content_wrap" role="main" data-offset="200" data-spy="scroll" data-target="#ancher_nav">
            <?php $r = 0;  while ( have_rows('sections') ) : the_row(); ?>
                <?php
                $r++;
                  //  echo the_row_index();
                ?>
                <?php include(locate_template('wp_setup/components/sections.php')); ?>

            <?php endwhile; ?><!-- while ( have_rows('sections') ) -->
             </main>
        <?php elseif (!is_front_page()  && have_rows('sections') ) : ?>
            <main id="sub_page" class="content_wrap" role="main" data-offset="200" data-spy="scroll" data-target="#ancher_nav">
                <h1 <?php echo hybrid_get_attr('entry-title  container'); ?>>
                    <?php the_title(); ?>
                </h1>
                <?php include(locate_template('wp_setup/components/content.php')); ?>
                  <?php $r = 0; while ( have_rows('sections') ) : the_row(); ?>
                <?php
                $r++;
                include(locate_template('wp_setup/components/sections.php')); ?>
                  <?php endwhile; ?>
            </main>
        <?php else : ?>
                <main id="singular_page" class="content_wrap" role="main" data-offset="200" data-spy="scroll" data-target="#ancher_nav">
                <h1 <?php echo hybrid_get_attr('entry-title , container'); ?>>
                    <?php the_title(); ?>
                </h1>
                <?php include(locate_template('wp_setup/components/content.php')); ?>

            </main>
        <?php endif; ?>
<?php get_footer(); ?>