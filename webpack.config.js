const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


var config = {

};

module.exports = function (env, argv) {
  if (argv.mode === 'development') {
    config.devtool = 'source-map';
  }
  return {

    entry: {
      resource_loader: "./src/resource_loader.jsx",
      script_loader: "./src/script_loader.js",
      style_loader: './src/style_loader.jsx'
    },
    output: {
      path: __dirname + "/core/js",
      filename: '[name].js',
      sourceMapFilename: '[name].map',
      chunkFilename: '[id].js'
    },
    devtool: argv.mode === 'development' ? 'cheap-eval-source-map' : 'source-map',


    module: {
      rules: [

        {
          test: /\.(scss)$/,
          exclude: /(node_modules|bower_components)/,
          use: [{
              loader: MiniCssExtractPlugin.loader, // inject CSS to page
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader', // Run post css actions
              options: {
                sourceMap: true,
                plugins: function () { // post css plugins, can be exported to postcss.config.js
                  return [
                   autoprefixer
                  ];
                }
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              } // compiles Sass to CSS
            }
          ]
        },
        {
          test: /\.js(x)$/,
          loader: "babel-loader", // Do not use "use" here
          exclude: /(node_modules|bower_components)/,
          query: {
            presets: ['react', 'es2015', 'stage-1']
          }
        },
        {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          loader: 'url-loader',
          exclude: /(node_modules|bower_components)/,
          options: {
            limit: 20000,
          },
        }
      ]

    },

    plugins: [
      new webpack.LoaderOptionsPlugin({
        context: '/',
        options: {
          eslint: {
            configFile: '.eslintrc',
            failOnWarning: false,
            failOnError: true
          }
        }
      }),
      new UglifyJSPlugin({
        sourceMap: true
      }),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "../css/[name].css",
        chunkFilename: "../css/[id].css"
      }),
      new webpack.ProvidePlugin({ // inject ES5 modules as global vars
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        'tilt':'tilt',
        Popper: [
          'popper.js', 'default'
        ],
        Tether: 'tether'
      })
    ]
  }
}
