<?php

/**
 * Posts After Singular Template
 *
 * Displays widgets for the After Singular dynamic sidebar if any have been added to the sidebar through the
 * widgets screen in the admin by the user.  Otherwise, nothing is displayed.
 *
 */

?>
<!-- <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>
 -->
<nav id="sidebar-default" class="sidebar-sticky  sidebar">
	<?php $logo = get_field('logo', 'option'); ?>
	<!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
 -->
	<section id="media_image-3" class="widget logo_container d-flex align-items-center">
		<a href="<?php echo home_url(); ?>">
			<img id="logo" src="<?php echo $logo['url'] ?>" width="323" height="408">
		</a>
	</section>

	<div class="sidebar_menu d-flex align-items-end flex-column">
		<?php $file = get_field('kostenkalkulation_pdf', 'option');
	if ($file) : ?>
		<section id="beispielkalkulation_box" class="widget beispielkalkulation pb-4 mt-auto">
			<div class="">
				<h5 class="sidebar-heading">
					Preise
				</h5>
				<a href="<?php echo $file['url']; ?>" download="<?php echo $file['title']; ?>"
				 class="sideaction">Download Pdf </a>
			</div>
		</section>
		<?php endif; ?>

		<section class="widget anfrage pt-4 pb-4">
			<div class="">
				<h5 class="sidebar-heading">
					Anfrage
				</h5>
<!-- 				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Contact_Modal" data-whatever="@mdo">Open modal for @mdo</button>
 -->				<a href="#" class="sideaction"  data-toggle="modal" data-target="#Contact_Modal" >Kontaktformular</a>
				<!-- <i class="fas fa-file-export"></i> -->
			</div>
		</section>

		<section id="contact_box" class=" widget  kontakt_info pt-4 pb-4 mb-auto">
			<div class="">
				<h5 class="sidebar-heading ">
				    <a href="#" class="sideaction"  data-toggle="modal" data-target="#Contact_Modal" >Kontakt</a>
					<!--Kontakt-->
				</h5>
				<ul class="nav flex-column">
					<li class="nav-item">
						<span class="link copy_able">
							<?php the_field('tel', 'option'); ?>
						</span>
					</li>
					<li class="nav-item">
						<span id="phone-sidebar"  class="link copy_able">
							<?php the_field('mobile', 'option'); ?>
						</span>
					</li>
					<li class="nav-item">

						<input readonly type="text" value="<?php the_field('mail', 'option'); ?>" class="allowCopy link copy_able">
						
					    <!--<span id="contact-sidebar" class="link copy_able">
							<?php //the_field('mail', 'option'); ?>
						</span>-->
						<?php /*<a class="link copy_able" href="<?php the_field('mail', 'option'); ?>">
							<?php the_field('mail', 'option'); ?>
						</a>*/ ?>
					</li>
				</ul>
			</div>
		</section>

		<section id="social" class="widget pt-4 pb-4">
			<ul class="fluid_row">
				<li class="social-item">
					<a class="fb" href="<?php the_field('facebook_url', 'option'); ?>" target="_blank" rel="noopener noreferrer">
						<i class="fab fa-facebook-f "></i>
					</a>
				</li>
				<li class="social-item">
					<a class="yt" href="<?php the_field('youtube_url', 'option'); ?>" target="_blank" rel="noopener noreferrer">
						<i class="fab fa-youtube "></i>
					</a>
				</li>
			</ul>

		</section>

		<section id="menu_box" class="widget mt-auto mb-auto">
			<div class="container">

				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="link copy_able" href="<?php the_field('impressum', 'option'); ?>" target="_blank" rel="noopener noreferrer">Impressum</a>

					</li>

				</ul>
			</div>


		</section>
  </div>
 <!-- <i class="fas fa-chevron-down .d-block .d-sm-none"></i> -->
</nav>
<div class="sidebar_background"></div>
