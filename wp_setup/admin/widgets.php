<?php
// Register and load the widget
function mf_load_widget() {
    register_widget( 'mf_widget' );
}
add_action( 'widgets_init', 'mf_load_widget' );
 
// Creating the widget 
class mf_widget extends WP_Widget {
 
function __construct() {
parent::__construct(

// Base ID of your widget
'mf_widget',
// Widget name will appear in UI
__('MF  Preisanfrage', 'mf_widget_domain'), 
// Widget description
array( 'description' => __( 'Custom Widget - Beispielkalkulation', 'mf_widget_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];
    ?>
   <div class="kontakt_info widget_content">
    <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Kontakt</span>
    </h5>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-phone-square"></i>
          <p>030 498 55 3 66</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-phone-square"></i>
         <p> 0176-23936477</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="mailto:pool@morphium-film.de">
          <i class="fas fa-envelope-square"></i>
         <p>pool (at) morphium-film.de</p> 
        </a>
      </li>
    </ul>
  </div>
  
  
  <div class="beispielkalkulation widget_content">
    <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>
        <?php echo __( 'Preisanfrage', 'mf_widget_domain' );?>
      </span>
    </h5>

    <div class="">
      <img src="<?php bloginfo('template_url');?>/src/img/pdf.png"></img>
    </div>
  </div>
 
  <div class="sidenav">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fab fa-facebook-square"></i>
          <i class="fab fa-youtube"></i>
          <i class="fab fa-linkedin-in"></i>
          <i class="fab fa-twitter"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          Impressum / Terms & Privacy
        </a>
      </li>
    </ul>
  </div>

  <?php echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
} else {
$title = __( 'Title', 'mf_widget_domain' );
}

// Widget admin form
?>
  <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">
      <?php _e( 'Title:' ); ?>
    </label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"
      type="text" value="<?php echo esc_attr( $title ); ?>" />
  </p>
  <?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here
?>