<div class="section_content  container">
  <div class="col col-md-5">
    <?php the_sub_field('column_1'); ?>
  </div>
  <div class="col col-md-7">
    <?php the_sub_field('column_2'); ?>
  </div>
</div>
