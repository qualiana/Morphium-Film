
    <!-- col-md-4 -->
    <div class="section_content media_content row">

        <div class="grid_container " id="video_grid" data-current="1" data-total="0">

            <div class="grid_controller " id="video_post_filter">
            <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                <?php $categories =
                        get_categories(array(
                        'hide_empty' => 0,
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'meta_key' => 'ba_checkbox_field_id'
                    )); ?>

                <div class="category pt-1 pb-1 mt-auto" data-cat="">
                    <a href="#"> Alle </a>
                </div>

                <?php foreach ($categories as $category) { ?>
                <div class="category pt-1 pb-1 " data-cat="<?php echo $category->slug; ?>">
                    <a href="#">
                        <?php echo $category->name; ?></a>
                </div>
                <?php } ?>

            </div>
            <!-- categories -->
        </div>
        <!-- grid_controller -->
           <div class=" row justify-content-md-center " id="loadvideos">     <!-- dynamic content -->
           </div>

                <a class="prev previuos_posts" id="prev_projectpage"  href="#">
                    <span class="icon-wrap">
                        <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                            <use xlink:href="#arrow-left-1">
                        </svg>
                    </span>
                </a>
                <a class="next next_posts" id="next_projectpage" href="#">
                    <span class="icon-wrap">
                        <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                            <use xlink:href="#arrow-right-1">
                        </svg>
                    </span>
                </a>
            <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
        </div>
    </div>
