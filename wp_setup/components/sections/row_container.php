

    <div class="section_content container-fluid"><!-- justify-content-center align-items-center d-flex -->
       <div class="row container align-items-center row_grid">
       <?php
        $args = array(
            'post_type' => get_sub_field('group'),
            'post_status' => array('publish'),
            //'posts_per_page' => get_sub_field('number_cards'),
            'meta_key' => 'featured',
            'posts_per_page' => 21,
            'meta_query'=> array(
                array(
                    'key' => 'featured',
                     'compare' => 'EXISTS'
                )
            ),


        );
        $the_query = new WP_Query($args);
        $cl = 0;
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $small_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
                $cl++; ?>
                    <div class="col seven_flow">
                      <figure data-tilt  class="kunde open_modal visible">
                          <img class="kunden_logo" src="<?php echo $small_image[0]; ?>" alt="Kunde : <?php the_title(); ?>"/>
                      </figure>
                    </div>

            <?php  } ?>

        <?php } ?>
         </div>
    </div>


<?php wp_reset_query(); ?>


 <?php wp_reset_postdata(); ?>
